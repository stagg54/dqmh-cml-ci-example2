# usEithis to build locally - runs through the whole test_build_buildpkg process.
# pass in the bulid number - needs to be supplied since normally supplied by CI pipeline.

set -eou pipefail # needed so script exits on errors.
SECONDS=0

# This line reads in all the variables in the gitlab CI file and creates the appropriate env variables.
# This allows us to define the vars once in the yml file
# and then run the build process locally using the same vars
# see https://unix.stackexchange.com/questions/539009/export-environment-variables-parsed-from-yaml-text-file
. <(sed -nr '/variables:/,$ s/  ([A-Z_]+): (.*)/\1=\2/ p' vars.yml)
# We set the full version here to whatever is passed in
# This is done after we read in the yaml file so we overwrite whatever was there
# clean up any remnants from previous runs.
# will all pass due to force -f
# not necessarily needed on build machine, but nice when running locally
# particularly for Unit test report as lunit just creates a new copy everytime
echo "Building antidoc documentation"
rm -rf "public"

#
HERE=$(cygpath -w $(pwd))


g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" vipc -- "$VIPC_PATH" -t "$VIPC_TIMEOUT" -v "$VIPC_LV_VERSION"
g-cli --timeout "$GCLI_LV_TIMEOUT" --lv-ver "$GCLI_LV_VERSION" --arch "$GCLI_LV_ARCH" antidoc -- -addon "lvproj" -out "$HERE\\public" -configpath "$HERE\\$ANTIDOC_CFG_PATH" -commit $(git log --pretty=format:'%h' -n 1) -pp "$HERE\\$LVPROJ_PATH" -t "$ANTIDOC_TITLE" -r html
mv "public\\Project-Documentation.html" "public\\index.html" # rename to index.html

echo "Total Script Time: $SECONDS"


